from math import gamma
import math
import gym
import numpy as np

import torch
import torch.nn as nn
from torch.distributions.normal import Normal
from torch.optim import Adam

import scipy.signal

from ActorCritic import ActorCritic
from Utility import InterruptHandler, SaverLoader, Logger




# computes mean and var on data
# taken from https://github.com/openai/baselines/blob/master/baselines/common/vec_env/vec_normalize.py
class RunningMeanStd(object):
    def __init__(self, init_mean=None, init_var=None, epsilon=1e-4, shape=()):
        self.mean = np.zeros(shape, 'float64')
        self.var = np.ones(shape, 'float64')

        if isinstance(init_mean,(np.ndarray)) : self.mean = init_mean
        if isinstance(init_mean,(np.ndarray)) : self.var = init_var

        self.count = epsilon

    def update(self, x):
        batch_mean = np.mean([x], axis=0)
        batch_var = np.var([x], axis=0)
        batch_count = 1
        self.update_from_moments(batch_mean, batch_var, batch_count)

    def update_from_moments(self, batch_mean, batch_var, batch_count):
        self.mean, self.var, self.count = update_mean_var_count_from_moments(
            self.mean, self.var, self.count, batch_mean, batch_var, batch_count)

def update_mean_var_count_from_moments(mean, var, count, batch_mean, batch_var, batch_count):
    delta = batch_mean - mean
    tot_count = count + batch_count

    new_mean = mean + delta * batch_count / tot_count
    m_a = var * count
    m_b = batch_var * batch_count
    M2 = m_a + m_b + np.square(delta) * count * batch_count / tot_count
    new_var = M2 / tot_count
    new_count = tot_count

    return new_mean, new_var, new_count


class DataBuffer:

    def __init__(self, obs_dim, act_dim, gamma=0.99, lam=0.95):

        self.obs_buf = [] # observations
        self.act_buf = [] # actions
        self.adv_buf = [] # advantages GAE (Generalized Advantages Estimator)
        self.rew_buf = [] # rewards (normalized)
        self.real_rew_buf = [] # rewards
        self.ret_buf = [] # targets of critics
        self.val_buf = [] # values returned by critic model
        self.logp_buf = [] # log likelihood of the taken action
        self.gamma, self.lam = gamma, lam
        self.ptr, self.path_start_idx, = 0, 0


    def discount_cumsum(self, x, discount):

        return scipy.signal.lfilter([1], [1, float(-discount)], x[::-1], axis=0)[::-1]


    def store(self, obs, act, rew, real_rew, val, logp):

        self.obs_buf.append(obs)
        self.act_buf.append(act)
        self.rew_buf.append(rew)
        self.real_rew_buf.append(real_rew)
        self.val_buf.append(val)
        self.logp_buf.append(logp)
        self.ptr += 1


    def finish_path(self, last_val=0):

        path_slice = slice(self.path_start_idx, self.ptr)
        rews = np.append(self.rew_buf[path_slice], last_val)
        vals = np.append(self.val_buf[path_slice], last_val)
        
        # the next two lines implement GAE-Lambda advantage calculation
        deltas = rews[:-1] + self.gamma * vals[1:] - vals[:-1]
        self.adv_buf.extend(self.discount_cumsum(deltas, self.gamma * self.lam)) 
        
        # the next line computes rewards-to-go, to be targets for the value function
        self.ret_buf.extend(self.discount_cumsum(rews, self.gamma)[:-1])
        
        self.path_start_idx = self.ptr

    
    def get_rewards(self):

        return np.array(self.real_rew_buf)


    def get(self, epsilon=1e-8):

        self.ptr, self.path_start_idx = 0, 0

        self.obs_buf = np.array(self.obs_buf)
        self.act_buf = np.array(self.act_buf)
        self.adv_buf = np.array(self.adv_buf)
        self.rew_buf = np.array(self.rew_buf)
        self.ret_buf = np.array(self.ret_buf)
        self.val_buf = np.array(self.val_buf)
        self.logp_buf = np.array(self.logp_buf)

        # normalize the advantages
        adv_mean, adv_std = np.mean(self.adv_buf), np.std(self.adv_buf)
        self.adv_buf = (self.adv_buf - adv_mean) / (adv_std + epsilon)

        # normalize the targets
        ret_mean, ret_std = np.mean(self.ret_buf), np.std(self.ret_buf)
        self.ret_buf = (self.ret_buf - ret_mean) / (ret_std + epsilon)

        data = dict(obs=self.obs_buf, act=self.act_buf, ret=self.ret_buf,
                    adv=self.adv_buf, logp=self.logp_buf)

        # reset the buffers for the next iteration
        self.obs_buf = []
        self.act_buf = []
        self.adv_buf = []
        self.rew_buf = []
        self.ret_buf = []
        self.val_buf = []
        self.logp_buf = []

        return {k: torch.as_tensor(v, dtype=torch.float32) for k,v in data.items()}



class PPO():

    def __init__(self, env_name, epochs, episodes_per_epoch,
                 clip_ob, clip_rew,
                 gamma, lam, clip_ratio,
                 pi_lr, vf_lr,
                 train_pi_iters, train_vf_iters,
                 hidden_sizes,
                 load_path, save_frequency,
                 render_mode
                ):
         


        self.interrupt_handler = InterruptHandler()
        self.logger = Logger()
        self.saver_loader = SaverLoader(save_frequency, load_path)


        self.epoch = 0      # current epoch
        self.epochs = epochs

        self.global_steps = 0 # array of ts
       
        self.episodes_per_epoch = episodes_per_epoch
        self.max_steps_per_episode = 100

        # values to clip
        self.clip_ob = clip_ob
        self.clip_rew = clip_rew

        # clip for the loss calculation
        self.clip_ratio = clip_ratio

        # iterations during training
        self.train_pi_iters = train_pi_iters
        self.train_vf_iters = train_vf_iters

        obs_mean = None
        obs_var = None
        rew_mean = None 
        rew_var = None

        actor_sizes = critic_sizes = hidden_sizes      

        self.render_mode = render_mode

        # load params from save file
        if load_path != '':
            try :
                checkpoint = self.saver_loader.load()
                self.episodes_per_epoch = checkpoint['episodes_per_epoch']
                gamma = checkpoint['gamma']
                lam = checkpoint['lambda']
                self.clip_ob = checkpoint['clip_ob']
                self.clip_rew = checkpoint['clip_rew']
                self.clip_ratio = checkpoint['clip_ratio']
                self.train_pi_iters = checkpoint['train_pi_iters']
                self.train_vf_iters = checkpoint['train_vf_iters']

                obs_mean = checkpoint['obs_mean']
                obs_var = checkpoint['obs_var']
                rew_mean = checkpoint['rew_mean']
                rew_var = checkpoint['rew_var']

                actor_sizes = checkpoint['actor_hidden_sizes']
                critic_sizes = checkpoint['critic_hidden_sizes']
                pi_lr = checkpoint['pi_lr']
                vf_lr = checkpoint['vf_lr']

                actor_state = checkpoint['actor_model_state_dict']
                critic_state = checkpoint['critic_model_state_dict']
                pi_state = checkpoint['pi_optimizer_state_dict']
                vf_state = checkpoint['vf_optimizer_state_dict']

                self.epoch = checkpoint['epoch']

                self.global_steps = checkpoint['timesteps'][-1] # re-start train from the last saved amount of ts

                

                print(load_path, "restored")
        
            except Exception as e:
                print("Could not restore file\n", e.with_traceback())
                exit()

        else:
            params = {
                'env'                : env_name,
                'episodes_per_epoch' : self.episodes_per_epoch,
                'gamma'              : gamma,
                'lambda'             : lam,
                'clip_ob'            : self.clip_ob,
                'clip_rew'           : self.clip_rew,
                'clip_ratio'         : self.clip_ratio,
                'train_pi_iters'     : self.train_pi_iters,
                'train_vf_iters'     : self.train_vf_iters,
                'actor_hidden_sizes' : actor_sizes,
                'critic_hidden_sizes': critic_sizes,
                'pi_lr'              : pi_lr,
                'vf_lr'              : vf_lr
            }
            self.saver_loader.update(params)
        

        self.logger.log_configuration(env_name, epochs, episodes_per_epoch, clip_ob, clip_rew, gamma, lam, clip_ratio, pi_lr, vf_lr, train_pi_iters, train_vf_iters, actor_sizes, critic_sizes, load_path, save_frequency, render_mode)

        # initialize environment
        self.env = gym.make(env_name)
        obs_dim = self.env.observation_space.shape
        act_dim = self.env.action_space.shape
            
        # to calculate the distribution and normalize/scale data
        self.obs_rms = RunningMeanStd(init_mean=obs_mean, init_var=obs_var, shape=obs_dim)
        self.rew_rms = RunningMeanStd(init_mean=rew_mean, init_var=rew_var, shape=(1,))

        self.buffer = DataBuffer(obs_dim, act_dim, gamma=gamma, lam=lam)

        # initialize actor and critic
        self.ac = ActorCritic(obs_dim, act_dim, actor_sizes, critic_sizes)

        self.pi_optimizer = Adam(self.ac.actor.parameters(), lr=pi_lr, eps=1e-5)
        self.vf_optimizer = Adam(self.ac.critic.parameters(), lr=vf_lr, eps=1e-5)

        if load_path != '':
            self.ac.actor.load_state_dict(actor_state)
            self.ac.critic.load_state_dict(critic_state)
            self.pi_optimizer.load_state_dict(pi_state)
            self.vf_optimizer.load_state_dict(vf_state)

    
    # compute the PPO policy loss with clipping 
    def compute_loss_pi(self, data):

        obs, act, adv, logp_old = data['obs'], data['act'], data['adv'], data['logp']

        # Policy loss
        pi, logp, ent = self.ac.get_action(obs, act)
        ratio = torch.exp(logp - logp_old)
        clip_adv = torch.clamp(ratio, 1-self.clip_ratio, 1+self.clip_ratio) * adv
        loss_pi = -(torch.min(ratio * adv, clip_adv)).mean()

        # # others clip function taken from: https://arxiv.org/abs/2012.02439
        # # PPOS - Proximal Policy Optimization Smoothed
        # alpha = 0.05
        # clip_adv_ppo = torch.clamp(ratio, 1-self.clip_ratio, 1+self.clip_ratio) 
        # MAX = -(alpha*torch.tanh(ratio-1)) + 1 + self.clip_ratio + alpha*math.tanh(self.clip_ratio)
        # MIN = -(alpha*torch.tanh(ratio-1)) + 1 - self.clip_ratio - alpha*math.tanh(self.clip_ratio) 
        # torch.where(clip_adv_ppo == 1 - self.clip_ratio, MAX, clip_adv_ppo)
        # torch.where(clip_adv_ppo == 1 + self.clip_ratio, MIN, clip_adv_ppo)
        # loss_pi_ppos = -(torch.min(ratio * adv, clip_adv_ppo * adv)).mean()

        # # PPORB - Proximal policy optimization with Rollback
        # alpha = 0.05
        # clip_adv_ppo = torch.clamp(ratio, 1-self.clip_ratio, 1+self.clip_ratio) 
        # MAX = -alpha*ratio + (1 + alpha)*(1 - self.clip_ratio)
        # MIN = -alpha*ratio + (1 + alpha)*(1 + self.clip_ratio) 
        # torch.where(clip_adv_ppo == 1 - self.clip_ratio, MAX, clip_adv_ppo)
        # torch.where(clip_adv_ppo == 1 + self.clip_ratio, MIN, clip_adv_ppo)
        # loss_pi_pporb = -(torch.min(ratio * adv, clip_adv_ppo * adv)).mean()


        # Useful extra info
        approx_kl = (logp_old - logp).mean().item()
        clipped = ratio.gt(1+self.clip_ratio) | ratio.lt(1-self.clip_ratio)
        clipfrac = torch.as_tensor(clipped, dtype=torch.float32).mean().item()
        pi_info = dict(kl=approx_kl, ent=ent, cf=clipfrac)

        return loss_pi, pi_info
        # return loss_pi_ppos, pi_info
        # return loss_pi_pporb, pi_info


    
    # compute the value loss
    def compute_loss_vf(self, data):
        
        obs, ret = data['obs'], data['ret']
        return ((self.ac.get_value(obs) - ret)**2).mean()


    # collect training data in the buffer by taking actions
    def collect(self, epsilon=1e-8):

        epoch_steps = 0            # number of steps taken during the current epoch
        episode_steps = 0           # number of steps taken during the current episode
        episode = 1                # current episode

        score_sum = 0              # sum of all the scores in the round 

        o = self.env.reset()

        while True:

            if self.render_mode : self.env.render()

            # normalize and clip the observation
            self.obs_rms.update(o)      # this will update the std and mean
            o = (o - self.obs_rms.mean) / np.sqrt(self.obs_rms.var + epsilon)       # normalize
            o = np.clip(o,-self.clip_ob, self.clip_ob)                              # clip

            a, v, logp = self.ac.step(torch.as_tensor(o, dtype=torch.float32))

            next_o, r, done, _ = self.env.step(a)

            real_r = r

            # scale and clip the reward
            self.rew_rms.update(r)
            r = r / np.sqrt(self.rew_rms.var + epsilon)                             # scale
            r = np.clip(r, -self.clip_rew, self.clip_rew)                           # clip

            self.buffer.store(o, a, r, real_r, v, logp)

            epoch_steps += 1
            episode_steps += 1
            self.global_steps += 1

            o = next_o
            
            terminate = done or episode_steps == self.max_steps_per_episode

            if terminate:

                _, last_v, _ = self.ac.step(torch.as_tensor(next_o, dtype=torch.float32))
                self.buffer.finish_path(last_v)

                score = np.sum(self.buffer.get_rewards()[epoch_steps - episode_steps:epoch_steps])

                # save the score
                self.saver_loader.store({'scores': score})

                
                score_sum += score
                average = score_sum / episode

                
                print("# Epoch: {}\t score: {:.2f}\t average: {:.2f}\t [{}/{}]    #".format(self.epoch, score, average, episode, self.episodes_per_epoch), 
                        end="\r", flush=True)
                
                # prepare for the next iteration
                o = self.env.reset()
                episode_steps = 0
                episode += 1
                if episode > self.episodes_per_epoch : break

            self.render_mode = self.interrupt_handler.handle(self.render_mode)


        # save the training current timesteps
        self.saver_loader.store({'timesteps': self.global_steps})  # it's the timestep at the end of this epoch
        
        # save the return average of this epoch
        self.saver_loader.store({'average': average})  # it's the return average at the end of episodes_per_epoch episodes (at the end of this epoch)
                
        
        print()




    # train on collected data
    def train(self):

        data = self.buffer.get()

        pi_l_old, pi_info_old = self.compute_loss_pi(data)
        pi_l_old = pi_l_old.item()
        v_l_old = self.compute_loss_vf(data).item()

        # Train policy with multiple steps of gradient descent
        for i in range(self.train_pi_iters):
            progress = int(i / (self.train_pi_iters + self.train_vf_iters) * 54)
            print("# Train... [" + progress * "=" + ">" + (53 - progress) * ' ' + "] #", end='\r', flush=True)

            self.pi_optimizer.zero_grad()
            loss_pi, pi_info = self.compute_loss_pi(data)
            kl = pi_info['kl']
            loss_pi.backward()
            self.pi_optimizer.step()

        # Value function learning
        for i in range(self.train_vf_iters):
            progress = int((i + self.train_pi_iters) / (self.train_pi_iters + self.train_vf_iters) * 54)
            print("# Train... [" + progress * "=" + ">" + (53 - progress) * ' ' + "] #", end='\r', flush=True)

            self.vf_optimizer.zero_grad()
            loss_v = self.compute_loss_vf(data)
            loss_v.backward()
            self.vf_optimizer.step()

        # print("#####################################################################")
    
        
        ent, kl = pi_info_old['ent'], pi_info['kl']
        delta_pi = loss_pi.item() - pi_l_old
        delta_vf = loss_v.item() - v_l_old

        data = {
            'entropy': ent,
            'kl': kl,
            'loss_pi': pi_l_old,
            'loss_vf': v_l_old,
            'delta_loss_pi': delta_pi,
            'delta_loss_vf': delta_vf
        }
        self.saver_loader.store(data)
        self.logger.store(data)
        
        self.logger.onboard(self.epoch)
        self.logger.onconsole()

        data = {
            'epoch': self.epoch,
            'actor_model_state_dict': self.ac.actor.state_dict(),
            'critic_model_state_dict': self.ac.critic.state_dict(),
            'pi_optimizer_state_dict': self.pi_optimizer.state_dict(),
            'vf_optimizer_state_dict': self.vf_optimizer.state_dict(),
            'obs_mean': self.obs_rms.mean,
            'obs_var': self.obs_rms.var,
            'rew_mean': self.rew_rms.mean,
            'rew_var': self.rew_rms.var
        }

        self.saver_loader.update(data)
        


    def run(self):
        
        while self.epoch < self.epochs:

            self.epoch += 1

            self.collect()
            self.train()

            self.saver_loader.save()

        # close and terminate
        self.env.close()
  

import argparse
from Utility import str2bool

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--env', type=str, default='Humanoid-v2')
    parser.add_argument('--epochs', type=int, default=100000)
    parser.add_argument('--episodes', type=int, default=50)
    parser.add_argument('--gamma', type=float, default=0.99)
    parser.add_argument('--lam', type=float, default=0.95)
    parser.add_argument('--clip', type=float, default=0.2)
    parser.add_argument('--pi_lr', type=float, default=3e-4)
    parser.add_argument('--vf_lr', type=float, default=1e-3)
    parser.add_argument('--hid', type=int, default=64)
    parser.add_argument('--l', type=int, default=2)
    parser.add_argument('--pi_iters', type=int, default=10)
    parser.add_argument('--vf_iters', type=int, default=10)
    # parser.add_argument('--target_kl', type=float, default=0.00)
    parser.add_argument('--load', type=str, default='')
    parser.add_argument('--save_freq', type=int, default=5)
    parser.add_argument('--render', type=str, default='false')

    args = parser.parse_args()

    args.render = str2bool(args.render)
    hidden_sizes = [args.hid]*args.l 

    ppo = PPO(args.env, epochs=args.epochs, episodes_per_epoch=args.episodes,
        clip_ob=10., clip_rew=10.,
        gamma=args.gamma, lam=args.lam, clip_ratio=args.clip,
        pi_lr=args.pi_lr, vf_lr=args.vf_lr,
        train_pi_iters=args.pi_iters, train_vf_iters=args.vf_iters,
        hidden_sizes=hidden_sizes,
        # target_kl=args.target_kl, 
        load_path=args.load, save_frequency=args.save_freq,
        render_mode=args.render
    )

    
    ppo.run()
