import torch
import torch.nn as nn
from torch.distributions.normal import Normal
from torch.optim import Adam

import numpy as np



def mlp(sizes, activation, output_activation=nn.Identity):
    layers = []
    for j in range(len(sizes)-1):
        act = activation if j < len(sizes)-2 else output_activation
        layers += [nn.Linear(sizes[j], sizes[j+1]), act()]
    return nn.Sequential(*layers)



class Actor(nn.Module):

    def __init__(self, obs_dim, act_dim, hidden_sizes_actor):
        super(Actor, self).__init__()

        log_std = -0.5 * np.ones(act_dim, dtype=np.float32)
        self.log_std = torch.nn.Parameter(torch.as_tensor(log_std))
        self.mean = mlp([obs_dim[0]] + list(hidden_sizes_actor) + [act_dim[0]], nn.Tanh)



class Critic(nn.Module):

    def __init__(self, obs_dim, hidden_sizes_critic):
        super(Critic, self).__init__()

        self.vf = mlp([obs_dim[0]] + list(hidden_sizes_critic) + [1], nn.Tanh)



class ActorCritic(nn.Module):

    def __init__(self, obs_dim, act_dim, hidden_sizes_actor, hidden_sizes_critic):

        super(ActorCritic, self).__init__()

        self.actor = Actor(obs_dim, act_dim, hidden_sizes_actor)
        self.critic = Critic(obs_dim, hidden_sizes_critic)


    def get_action(self, obs, action=None):

        action_mean = self.actor.mean(obs) 
        action_std = torch.exp(self.actor.log_std)
        probs = Normal(action_mean, action_std)
        if action is None:
            action = probs.sample()
        return action, probs.log_prob(action).sum(-1), probs.entropy().mean().item()


    def get_value(self, obs):
        
        return torch.squeeze(self.critic.vf(obs), -1)


    def step(self, obs):

        with torch.no_grad():
            a, logp_a, _ = self.get_action(obs)
            v = self.get_value(obs)
        return a.numpy(), v.numpy(), logp_a.numpy()


    



