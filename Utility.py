import torch
from torch.utils import tensorboard
from torch.utils.tensorboard import SummaryWriter, writer

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

import os


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    RESET = '\033[0m' #RESET COLOR


    
class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        else:
            cls._instances[cls].__init__(*args, **kwargs)
        return cls._instances[cls]

# Log in console/tensorboard    
class Logger(object):

    __metaclass__ = Singleton

    def __init__(self):

        self.writer = SummaryWriter()
        self.data = {}

    
    # Log on console
    def log(self, msg):

        print(msg)
    

    def onboard(self, epoch) :

        for key in self.data :
            self.writer.add_scalar(key, self.data[key], epoch - 1)

        self.writer.close()

    # updates local data to print it
    def store(self, data) :

        self.data.update(data)
            

    def onconsole(self, tabular_mode = False) :

        print()
        print("#####################################################################")

        if tabular_mode :
        
            msg = ""
            for key in self.data:
                num_spaces = 0 if len(key) > 4 else 5 - len(key)
                msg+= key + num_spaces*" " + "\t"
                
            msg+=" #\n# "

            for key in self.data:
                num_spaces = 0 if len(key) < 5 else len(key) - 5
                msg+= "{:.2f}".format(self.data[key]) + num_spaces*" " + "\t"
                
            print("# " + msg + " #")

        else :
            max_key_len = max(len(k) for k in self.data)
            for key in self.data:
                print("# " + key + "{:.2f}".format(self.data[key]).rjust(52 + max_key_len - len(key)) + " #") 
 
        print("#####################################################################")
    

    def log_configuration(self, env_name, epochs, episodes_per_epoch, clip_ob, clip_rew, gamma, lam, clip_ratio, pi_lr, vf_lr, train_pi_iters, train_vf_iters, actor_sizes, critic_sizes, load_path, save_frequency, render_mode):

        print("#####################################################################")
        print("################### " + bcolors.OKCYAN + "Proximal Policy Optimization" + bcolors.RESET + " ####################") 
        print("#####################################################################")
    
        print("#### " + bcolors.OKCYAN + "Enviroment: " + bcolors.WARNING + env_name.rjust(47) + bcolors.RESET + " ####")
        print("#####################################################################")
        print("####################### Training configuration ######################")
        print("#####################################################################")
        print("#### " + bcolors.OKCYAN + "Iterations: " + bcolors.WARNING + str(epochs).rjust(47) + bcolors.RESET + " ####")
        print("#### " + bcolors.OKCYAN + "Episodes for iteration: " + bcolors.WARNING + str(episodes_per_epoch).rjust(35) + bcolors.RESET + " ####")
        print("#### " + bcolors.OKCYAN + "Actor Networks size: " + bcolors.WARNING + str(actor_sizes).rjust(38) + bcolors.RESET + " ####")
        print("#### " + bcolors.OKCYAN + "Critic Networks size: " + bcolors.WARNING + str(critic_sizes).rjust(37) + bcolors.RESET + " ####")
        print("#### " + bcolors.OKCYAN + "Gamma: " + bcolors.WARNING + str(gamma).rjust(52) + bcolors.RESET + " ####")
        print("#### " + bcolors.OKCYAN + "Lambda: " + bcolors.WARNING + str(lam).rjust(51) + bcolors.RESET + " ####")
        print("########################## Clipping factors #########################")
        print("#### " + bcolors.OKCYAN + "- observation: " + bcolors.WARNING + str(clip_ob).rjust(44) + bcolors.RESET + " ####")
        print("#### " + bcolors.OKCYAN + "- rewards: " + bcolors.WARNING + str(clip_rew).rjust(48) + bcolors.RESET + " ####")
        print("#### " + bcolors.OKCYAN + "- policy: " + bcolors.WARNING + str(clip_ratio).rjust(49) + bcolors.RESET + " ####")
       

        print("######################## Actor configuration ########################")
        print("#### " + bcolors.OKCYAN + "- iterations: " + bcolors.WARNING + str(train_pi_iters).rjust(45) + bcolors.RESET + " ####")
        print("#### " + bcolors.OKCYAN + "- learning rate: " + bcolors.WARNING + str(pi_lr).rjust(42) + bcolors.RESET + " ####")
        
        print("####################### Critic configuration ########################")
        print("#### " + bcolors.OKCYAN + "- iterations: " + bcolors.WARNING + str(train_vf_iters).rjust(45) + bcolors.RESET + " ####")
        print("#### " + bcolors.OKCYAN + "- learning rate: " + bcolors.WARNING + str(vf_lr).rjust(42) + bcolors.RESET + " ####")

        
        print("#####################################################################")
        print("####################### General configuration #######################")
        print("#####################################################################")
        if load_path != '' : print("#### " + bcolors.OKCYAN + "- loading path: " + bcolors.WARNING + str(load_path).rjust(43) + bcolors.RESET + " ####")
        else: print("#### " + bcolors.OKCYAN + "- loading path: " + bcolors.WARNING + "Model from scratch".rjust(43) + bcolors.RESET + " ####")
        print("#### " + bcolors.OKCYAN + "- render: " + bcolors.WARNING + str(render_mode).rjust(49) + bcolors.RESET + " ####")
        print("#### " + bcolors.OKCYAN + "- model save frequency: " + bcolors.WARNING + str(save_frequency).rjust(35) + bcolors.RESET + " ####")

        print("#####################################################################")
    



from datetime import datetime
import os

dirname = os.path.dirname(__file__)
save_dirname = os.path.join(dirname, "models")
plots_dirname = os.path.join(save_dirname, "plots")
if not os.path.exists(save_dirname):
    os.makedirs(save_dirname)
if not os.path.exists(plots_dirname):
    os.makedirs(plots_dirname)

# Load/Save model from/on disk 
class SaverLoader(object):
    __metaclass__ = Singleton

    def __init__(self, save_frequency, load_path):

        self.save_frequency = save_frequency
        self.count = 0
        self.data = {}

        if load_path != '':
            self.path = os.path.join(save_dirname, load_path)
            return

        date = str(datetime.now()).replace(" ", "_")
        date = date[0:date.index('.')]
        self.path = os.path.join(save_dirname, date + '.pt')


    def load(self) :

        return torch.load(self.path)

    # save the stored and updated values from the current dictionary (data) to the current model
    # and cleans the local dictionary (data)
    def save(self) :

        self.count += 1
        if self.count != self.save_frequency : return
        self.count = 0

        try :
            checkpoint = torch.load(self.path)

            for key in checkpoint :

                if key in self.data :
                    if isinstance(checkpoint[key], list) :
                        checkpoint[key].extend(self.data[key])
                    else :
                        checkpoint[key] = self.data[key]

        except :
            checkpoint = self.data

        torch.save(checkpoint, self.path)

        self.data = {}

    
    def plot(self, labels, time_label, save=True, max_time=-1) :

        try: 
            # print("Loading data...")
            data = self.load()

        except :
            raise Exception("Can't load data path")

        # print("Data Loaded.")
        # print("Plotting data...")

        if data[time_label] == None : 
            raise Exception("Time label is not in stored data")

        pdf = None
        save_path = None

        if save : 
            filename = os.path.splitext(os.path.basename(self.path))[0]
            save_path = os.path.join(plots_dirname, filename + ".pdf")
            pdf = PdfPages(save_path)

        # crops the time axis to the max_time if max_time > 0
        time_axis = []
        if max_time < 0 : time_axis = data[time_label]
        else :
            for time in data[time_label] :
                if time <= max_time : time_axis.append(time)

        for key in labels :

            if key not in data : continue

            fig = plt.figure()
            # plt.plot(range(1, len(data[key]) + 1), data[key])
            plt.plot(time_axis, data[key][0:len(time_axis)])

            plt.xlabel(labels[key])
            plt.ylabel(key)

            if save : pdf.savefig()
            plt.close(fig)


        # print("Data plotted.")

        if save : 
            pdf.close()
            # print("Data saved in", save_path)
            
            
            import subprocess, sys
            opener ="open" if sys.platform == "darwin" else "xdg-open"
            subprocess.call([opener, save_path])
            

    # adds data to target key (for array of value) (e.g. loss values, etc.)
    # or creates it if not exists
    def store(self, data) :

        if self.save_frequency == 0 : return
        
        for key in data :

            if key in self.data :
                self.data[key].append(data[key])

            else :
                self.data[key] = [data[key]]
    
    # sobstitudes data to target kay (for single value) (e.g. epoch, gamma, etc.)
    # or creates it if not exists
    def update(self, data) :

        if self.save_frequency == 0 : return

        self.data.update(data)


    
def str2bool(v):

    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')



import signal

class InterruptHandler:
    """Gracefully exit program on CTRL-C."""
    def __init__(self):

        self.kill_now = False
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)


    def exit_gracefully(self, signum, frame):

        self.kill_now = True


    # this function is used to check and handle interruptions of the execution
    def handle(self, render_mode):

        if not self.kill_now : return render_mode

        print()
        render_mode_phrase = ''
        if not render_mode : render_mode_phrase = '\nRender (r)'
        i = input('Quit (q) ' + render_mode_phrase + ' \nReturn ([n])\n') 
        if i == 'q':
            exit()
        elif i == 'r':
            render_mode = True
        self.kill_now = False

        return render_mode
