# load a model
# generates plot of data
# render the environment

import gym
import numpy as np
import torch

from ActorCritic import ActorCritic
from Utility import SaverLoader, Logger

import argparse


def test(path, save, max_timesteps):

    saver_loader = SaverLoader(0, path)

    checkpoint = None
    try :
        checkpoint = saver_loader.load()

        env_name = checkpoint['env']

        actor_sizes = checkpoint['actor_hidden_sizes']
        critic_sizes = checkpoint['critic_hidden_sizes']
        actor_state = checkpoint['actor_model_state_dict']
        critic_state = checkpoint['critic_model_state_dict']

        obs_mean = checkpoint['obs_mean']
        obs_var = checkpoint['obs_var']
        clip_ob = checkpoint['clip_ob']

    except Exception as e:
        print("Failed loading the model\n", e.with_traceback())
        exit()

    try :
        time_label = 'timesteps'
        saver_loader.plot({
            'average': time_label,
            'entropy': time_label,
            'kl': time_label,
            'loss_pi': time_label,
            'loss_vf': time_label,
            'delta_loss_pi': time_label,
            'delta_loss_vf': time_label
        }, time_label, save, max_time=max_timesteps)

    except Exception as e:
        print(e.with_traceback())


    # logger = Logger()
    # logger.log_configuration()

    # init env
    env = gym.make(env_name)
    obs_dim = env.observation_space.shape
    act_dim = env.action_space.shape

    ac = ActorCritic(obs_dim, act_dim, actor_sizes, critic_sizes)

    # load model
    ac.actor.load_state_dict(actor_state)
    ac.critic.load_state_dict(critic_state)

    o = env.reset()

    for i in range(1000):

        env.render()

        # normalize and clip the observation
        o = (o - obs_mean) / np.sqrt(obs_var + 1e-8)          # normalize
        o = np.clip(o,-clip_ob, clip_ob)                              # clip

        a, _, _ = ac.step(torch.as_tensor(o, dtype=torch.float32))

        o, _, done, _ = env.step(a)

        if done: env.reset()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str, help="path to load", required=True)
    parser.add_argument('--plot', type=str, help="save the plot? [y/n]", default="y")
    parser.add_argument('--maxtime', type=float, help="max timestamps that are plotted", default=-1)
    args = parser.parse_args()

    test(args.path, args.plot == 'y', args.maxtime)